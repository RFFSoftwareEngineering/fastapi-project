from fastapi import FastAPI, HTTPException, Depends
from pydantic import BaseModel
from typing import List
import sqlite3
import threading

app = FastAPI()


# Função para obter a conexão com o banco de dados SQLite específica para cada thread
def get_db():
    if not hasattr(threading.current_thread(), "sqlite_connection"):
        threading.current_thread().sqlite_connection = sqlite3.connect('financas.db')
        threading.current_thread().sqlite_connection.row_factory = sqlite3.Row
    return threading.current_thread().sqlite_connection


# Criar a tabela de transações se ela não existir
with get_db() as conn:
    cursor = conn.cursor()
    cursor.execute('''
    CREATE TABLE IF NOT EXISTS transacoes (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        descricao TEXT,
        valor REAL,
        categoria TEXT
    )
    ''')


# Modelo para representar uma transação financeira
class Transacao(BaseModel):
    descricao: str
    valor: float
    categoria: str


# Rota para adicionar uma nova transação
@app.post("/transacoes/", response_model=Transacao)
def criar_transacao(transacao: Transacao, db: sqlite3.Connection = Depends(get_db)):
    with db:
        cursor = db.cursor()
        cursor.execute('INSERT INTO transacoes (descricao, valor, categoria) VALUES (?, ?, ?)',
                       (transacao.descricao, transacao.valor, transacao.categoria))
        transacao_id = cursor.lastrowid
    return {**transacao.dict(), "id": transacao_id}


# Rota para listar todas as transações
@app.get("/transacoes/", response_model=List[Transacao])
def listar_transacoes(db: sqlite3.Connection = Depends(get_db)):
    with db:
        cursor = db.cursor()
        cursor.execute('SELECT * FROM transacoes')
        transacoes = cursor.fetchall()
    return [{"id": t["id"], "descricao": t["descricao"], "valor": t["valor"], "categoria": t["categoria"]} for t in transacoes]


# Rota para listar transações por categoria
@app.get("/transacoes/{categoria}", response_model=List[Transacao])
def listar_transacoes_por_categoria(categoria: str, db: sqlite3.Connection = Depends(get_db)):
    with db:
        cursor = db.cursor()
        cursor.execute('SELECT * FROM transacoes WHERE categoria=?', (categoria,))
        transacoes = cursor.fetchall()
    if not transacoes:
        raise HTTPException(status_code=404, detail="Categoria não encontrada")
    return [{"id": t["id"], "descricao": t["descricao"], "valor": t["valor"], "categoria": t["categoria"]} for t in transacoes]
